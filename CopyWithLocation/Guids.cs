﻿// Guids.cs
// MUST match guids.h
using System;

namespace Custis.CopyWithLocation
{
    static class GuidList
    {
        public const string guidCopyWithLocationPkgString = "8aa6c454-2744-4210-aec0-5f81c4ab396e";
        public const string guidCopyWithLocationCmdSetString = "2bc6e854-f05e-4adb-82d2-0aa19eefebd0";

        public static readonly Guid guidCopyWithLocationCmdSet = new Guid(guidCopyWithLocationCmdSetString);
    };
}