﻿// Guids.cs
// MUST match guids.h
using System;

namespace CUSTIS.BatchReplace
{
    static class GuidList
    {
        public const string guidBatchReplacePkgString = "b16b0c86-1acd-47ea-844b-bdad7f2980a2";
        public const string guidBatchReplaceCmdSetString = "3c36e254-1da2-4209-8328-d1c0fbd123ed";
        public const string guidToolWindowPersistanceString = "5be3a618-b771-48bb-b761-4da6d817b972";

        public static readonly Guid guidBatchReplaceCmdSet = new Guid(guidBatchReplaceCmdSetString);
    };
}