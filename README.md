# CopyWithLocation_1.2_Modify
【VS扩展插件项目（带地址复制）】
【原项目地址：https://bitbucket.org/ishatalkin/visual-studio-extensions】
【功能：Copies selected code with its location (class name, method name, line numbers)】
【VS插件库地址：https://marketplace.visualstudio.com/items?itemName=IgorShatalkin.CopyWithLocation】
【Version：1.2  |||  Last updated：2017/7/13 下午6:11:47  |||  Publisher： Igor Shatalkin】
